<?php

namespace App\Http\Controllers;

use App\Models\Enderecos;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

class OpenApi {}

class EnderecosController extends Controller
{

    public function showAllEnderecos()
    {
        return response()->json(Enderecos::all());
    }

    public function showOneEnderecos($id)
    {
        return response()->json(Enderecos::find($id));
    }

    public function create(Request $request)
    {   
        $data = $request->all(); 

        $this->validate($request, [
            'cep' => 'required',
            'numero' => 'required',
            'client_id' => 'required'
        ]);

        $cep = $data['cep'];
        $client_id = $data['client_id'];

        $url = "http://viacep.com.br/ws/".$cep."/xml/";

        $xml = simplexml_load_file($url);
        $xml = json_encode($xml);
        $xml = json_decode($xml,TRUE);

        // if(isset($xml['erro'])){
        //     return response('CEP Inválido', 400);
        //  }
        
        array_splice($xml, -4);

        if(empty($xml['complemento'])){
           $xml['complemento'] = '';
        }

        $xml['client_id'] = $client_id;
        $xml['numero'] = $data['numero'];

        $enderecos = Enderecos::create($xml);

        return response()->json($xml, 201);
    }

    public function update($id, Request $request)
    {
        $enderecos = Enderecos::findOrFail($id);
        $enderecos->update($request->all());

        return response()->json($enderecos, 200);
    }

    public function delete($id)
    {
        Enderecos::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}