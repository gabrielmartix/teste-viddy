<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    
    public function showAllClient()
    {
        return response()->json(Client::all());
    }

    public function showOneEnderecos($id)
    {
        return response()->json(Client::find($id));
    }

    public function create(Request $request)
    {
        $client = Client::create($request->all());

        return response()->json($client, 201);
    }

    public function update($id, Request $request)
    {
        $client = Client::findOrFail($id);
        $client->update($request->all());

        return response()->json($client, 200);
    }

    public function delete($id)
    {
        Client::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}