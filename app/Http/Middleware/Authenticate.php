<?php

namespace App\Http\Middleware;

use Closure;
use Auth0\SDK\Auth0;
use Auth0\SDK\Configuration\SdkConfiguration;


class Authenticate
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->bearerToken();
        if(!$token) {
        return response()->json('No token provided', 401);
        }

        $this->validateToken($token);

        return $next($request);
    }

    public function validateToken($token)
    {
    
        return TRUE;
            // $vtoken = new Token([
            //     'domain' => env['AUTH0_DOMAIN'],
            //     'clientId' => '63dbfd0a94ba76eae934ba31',
            //     'clientSecret' => env['AUTH0_CLIENT_SECRET'],
            //     'tokenAlgorithm' => 'HS256',
            // ], $token, \Auth0\SDK\Token::TYPE_ID_TOKEN);
            
            // // Verify the token: (This will throw an \Auth0\SDK\Exception\InvalidTokenException if verification fails.)
            // $token->verify();
            
            // // Validate the token claims: (This will throw an \Auth0\SDK\Exception\InvalidTokenException if validation fails.)
            // $token->validate();
    
    }
}