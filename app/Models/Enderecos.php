<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enderecos extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'cep', 'logradouro', 'numero', 'bairro', 'localidade', 'uf', 'complemento', 'client_id'
     ];

     protected $hidden = [];
}
