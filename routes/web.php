<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api', 'middleware' => 'auth'], function () use ($router) {
    $router->get('client',  ['uses' => 'ClientController@showAllclient']);
  
    $router->get('client/{id}', ['uses' => 'ClientController@showOneClient']);
  
    $router->post('client', ['uses' => 'ClientController@create']);
  
    $router->delete('client/{id}', ['uses' => 'ClientController@delete']);
  
    $router->put('client/{id}', ['uses' => 'ClientController@update']);
});

  $router->group(['prefix' => 'api', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/enderecos',  ['uses' => 'EnderecosController@showAllEnderecos']);
  
    $router->get('enderecos/{id}', ['uses' => 'EnderecosController@showOneEnderecos']);
  
    $router->post('enderecos', ['uses' => 'EnderecosController@create']);
  
    $router->delete('enderecos/{id}', ['uses' => 'EnderecosController@delete']);
  
    $router->put('enderecos/{id}', ['uses' => 'EnderecosController@update']);
});